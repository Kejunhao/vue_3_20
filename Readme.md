**先运行vue_api_server后端项目**

**再运行vue_shop前端项目**



## 后端

 npm install   安装node包

node run app.js

运行成功界面如下，不报错即可

![image-20200124100326720](C:\Users\Tecna1205\AppData\Roaming\Typora\typora-user-images\image-20200124100326720.png)

关于数据库配置：

首先将db目录下的mydb.sql放在数据库中运行一下，生成众多表格后，修改config目录下的default.json文件，配置数据库的用户名，密码以及端口号



## 前端

npm install

npm run serve



App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.114.1:8080/



输入地址，项目运行成功后截图

![image-20200124100907388](C:\Users\Tecna1205\AppData\Roaming\Typora\typora-user-images\image-20200124100907388.png)



![image-20200124100923376](C:\Users\Tecna1205\AppData\Roaming\Typora\typora-user-images\image-20200124100923376.png)